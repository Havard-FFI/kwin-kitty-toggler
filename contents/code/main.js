function toggleClient()
{
    const clients = workspace.clientList();

    let client = null;

    for (var i = 0; i < clients.length; i++) {
        let resource_name = clients[i].resourceName;

        if (resource_name == "kitty")
        {
            client = clients[i];
            break;
        }
    }

    if (client)
    {
        if (client.minimized)
        {
            client.setMaximize(true, true);
            client.keepAbove = false;
            client.keepBelow = false;
            workspace.activeClient = client;
        } else {
	    if(workspace.activeClient != client)
	    {
	        workspace.activeClient = client;
	    } else {
                client.minimized = true;
	    }
        }
    } else {
    }
}

function toggleKeepassXC()
{
    const clients = workspace.clientList();

    let client = null;

    for (var i = 0; i < clients.length; i++) {
        let resource_name = clients[i].resourceName;
        //console.info(resource_name)
        if (resource_name == "keepassxc")
        {
            client = clients[i];
            break;
        }
    }

    if (client)
    {
        if (client.minimized)
        {
            //client.setMaximize(true, true);
            workspace.activeClient = client;
        } else {
	    if(workspace.activeClient != client)
	    {
	        workspace.activeClient = client;
	    } else {
                client.minimized = true;
	    }
        }
    } else {
    }
}

registerShortcut("ToggleKittyWindow", "Toggle Kitty Window", "F12", toggleClient);
registerShortcut("ToggleKeepassXCWindow", "Toggle KeepassXC Window", "Meta+K", toggleKeepassXC);
